
// any random plain ol' functions that can be refactored out to save space in the main file.
// Everyone imports lib so also put custom types here.
extern crate nalgebra as na;
use na::{SMatrix, Vector3};

pub const NODES: usize = 16;

pub type Time = SMatrix<f32, 4, 4>;
pub type World = SMatrix<f32, NODES, 4>;
pub type Kernel = Vector3<f32>;
pub type Bytes64 = [u8; 64];

pub fn _print_type_of<T>(_: &T) {
        println!("{}", std::any::type_name::<T>())
}





pub fn _sq(x: f32) -> f32 {
    return x*x;
}
