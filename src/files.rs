// manage reading and writing to files.
use std::fs::{File, OpenOptions};
use std::io::{Read, Write};
use crate::lib::{Bytes64};

use glob::glob;


pub fn write_new(bytes: &[u8; 64]) {
  let filename = "foo.bin";
  let mut file = File::create(filename).unwrap();
  file.write_all(bytes).unwrap()
}

pub fn next_number_file() -> i32 {
  // returns next not-taken file number in places/ directory
  let mut num = 0;
  for entry in glob("places/*").expect("Failed to read glob pattern") {
    match entry {
      Ok(path) => {
        let number_file = path.file_name()
          .expect("broken file").to_str().expect("broken string")
          .parse::<i32>();
        match number_file {
          Ok(x) => {
            if x > num {
              num = x;
            }
          },
          Err(_) => {},
        }
      },
      Err(e) => println!("{:?}", e),
    }
  }
  return num + 1
}

pub fn my_write(filename: &str, bytes: &Bytes64) {
  let try_file = OpenOptions::new().write(true)
                         .create_new(true)
                         .open(filename);
  match try_file {
    Ok(mut file) => {
      file.write_all(bytes).unwrap();
      println!("Suceeded at writing to {}", filename);
    },
    Err(e) => println!("failed at open a file, Error: {:?}", e),
  }
}

  
pub fn write_next_number_file(bytes: &[u8; 64]) {
  let dest_filename = format!("places/{}", next_number_file().to_string());
  my_write(&dest_filename, bytes);
}

pub fn write_file(filename: &str, bytes: &[u8; 64]) {
  let dest_filename = ["places/", filename].join("");
  my_write(&dest_filename, bytes);

}

pub fn read_file(filename: &str) -> Option<Bytes64> {
  let src_filename = ["places/", filename].join("");
  match File::open(src_filename) {
    Ok(mut f) => {
      let mut buf : [u8; 64] = [0; 64];
      f.read_exact(&mut buf).unwrap();
      return Some(buf)
    },
    Err(e) => {
      println!("Error opening file: {:?}", e);
      return None;
    },
  }
}

pub fn list_files(location: u8) {
  // TODO use location
  for entry in glob("places/*").expect("Failed to read glob pattern") {
    match entry {
      Ok(path) => {
          println!("- {}", path.file_name().expect("broken file").to_str().expect("broken string"));
      }
      Err(e) => println!("{:?}", e),
    }
  }
}
