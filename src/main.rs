extern crate nalgebra as na;

mod lib;// helpers and type definitions and such
mod con;// midi and audio api
mod gen;// math and simulation and event response
mod files;// reading and writing persistent files

fn main() {
  // initialize simulation state and get closures to retrieve samples while transmitting midi events and such
  let instrument = gen::Instrument::init();
  println!("instrument {:?}", instrument);

	println!("Running file garbgge");
	//files::next_number_file
	files::list_files(0);
	println!("Next num file: {}", files::next_number_file());
  // initialize jack connections (audio only)
  con::init(instrument);

  // initialize jack-compatible midir connections
  
  // sleep

}
