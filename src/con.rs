
// connection code - links up midi and audio stuff from the real world
// to the math world described in gen.rs
// going from this ecample:
// https://github.com/RustAudio/rust-jack/blob/main/examples/sine.rs
use crossbeam_channel::bounded;
use std::io;
use crate::gen::{Instrument};
use crate::files::{list_files};
use std::sync::mpsc::sync_channel;
use std::convert::From;


const MAX_MIDI: usize = 3;

fn dispatch_midi(bytes: &[u8], instrument: &mut Instrument) {
	if bytes[0] == 176 {
		let raw_pos = bytes[1];
		if raw_pos >= 3 && raw_pos <= 11 {
			// slider
			let pos = raw_pos - 3;
			let value = bytes[2];
			instrument.set_param(pos.into(), (value as f32) / 128.0);
		} else if raw_pos >= 23 && raw_pos <= 31 {
			// button
			let pos = raw_pos - 23;
			let is_pressed = bytes[2] != 0;
			if is_pressed {
				instrument.scramble_param(pos.into());
			}
		} else if raw_pos >= 14 && raw_pos <= 22 {
			// knob
			let pos = raw_pos - 14;
			let value = bytes[2];
			instrument.set_param_scale(pos.into(), (2.0_f32).powf((value as f32) / 32.0));
		} else if raw_pos == 1 {
			// scramble all button
			let is_pressed = bytes[2] != 0;
			if is_pressed {
				for pos in 0..9 {
					instrument.scramble_param(pos as usize);
				}
			}
		} else if raw_pos == 2 {
			// pluck
			let is_pressed = bytes[2] != 0;
			if is_pressed {
				instrument.pluck(0.3);
			}
		} else if raw_pos == 67 {
			// write to file
			// will prompt for user to type in a name, warns if name already exists
			let is_pressed = bytes[2] != 0;
			if is_pressed {
				instrument.write_time_diff_to_file();
			}
		} else if raw_pos == 64 {
			// read from file
			let is_pressed = bytes[2] != 0;
			if is_pressed {
				instrument.read_time_diff_into_slider("places/1");
			}
		}
	}
	//Midi { time: 350, len: 3, data: [176, 7, 3] }
}

// Implement midi copy container and methods
// a fixed size container to copy data out of real-time thread
#[derive(Copy, Clone)]
struct MidiCopy {
    len: usize,
    data: [u8; MAX_MIDI],
    time: jack::Frames,
}
impl From<jack::RawMidi<'_>> for MidiCopy {
    fn from(midi: jack::RawMidi<'_>) -> Self {
        let len = std::cmp::min(MAX_MIDI, midi.bytes.len());
        let mut data = [0; MAX_MIDI];
        data[..len].copy_from_slice(&midi.bytes[..len]);
        MidiCopy {
            len,
            data,
            time: midi.time,
        }
    }
}
impl std::fmt::Debug for MidiCopy {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "Midi {{ time: {}, len: {}, data: {:?} }}",
            self.time,
            self.len,
            &self.data[..self.len]
        )
    }
}


pub fn init(mut instrument: Instrument) {

	// open the client
  let (client, _status) =
    jack::Client::new("vibes", jack::ClientOptions::NO_START_SERVER).expect("Did you forget to start qjackctl?");

  // software midi channel
  let (sender, receiver) = sync_channel(64);

  // get port for midi_in
  let midi_in = client
      .register_port("midi_in", jack::MidiIn::default())
      .unwrap();

  // get port for audio output
  let mut out_port = client
    .register_port("music_out", jack::AudioOut::default())
    .unwrap();

	// midi printing thread (non-realtime)
	std::thread::spawn(move || {
			while let Ok(m) = receiver.recv() {
					println!("{:?}", m);
			}
	});

  // realtime jack handler, handles midi events and audio out and keyboard input
  let sample_rate = client.sample_rate();
  let frame_t = 1.0 / sample_rate as f64;
  let mut time = 0.0;
  let (tx, rx): (crossbeam_channel::Sender<String>, crossbeam_channel::Receiver<String>) = bounded(1_000_000);
  let process = jack::ClosureProcessHandler::new(
			move |_: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {

			// get midi and send to tube for printing ang dispatch
      // TODO dispatch midi by time instead of immidiately for less jitter
			let show_p = midi_in.iter(ps);
			for e in show_p {
				let c: MidiCopy = e.into();
				let _ = sender.try_send(c);
				dispatch_midi(e.bytes, &mut instrument);
			}

			// TODO get mic input buffer

      // Get output buffer for writing
      let out = out_port.as_mut_slice(ps);

      // Check command requests
      while let Ok(s) = rx.try_recv() {
        let dest_file = s.trim();
        println!("Good job {}", dest_file);
        if dest_file == "ls" {
          // magic word to list available files
          list_files(0);
        } else if dest_file.chars().nth(0).unwrap() == '!' {
          // bang causes us to write instead of read
          let source_file = &dest_file[1..];
          instrument.read_time_diff_into_slider(&source_file);
        } else if dest_file.chars().nth(0).unwrap() == '@' {
          // @ causes us to set the destination slider [from 1-9]
          let target_slider_str = &dest_file[1..];
          match target_slider_str.parse::<i32>() {
            // x-1 so user can use 1 based counting
            Ok(x) => instrument.set_dest_slider(x-1),
            Err(e) => println!("Error setting dest slider: {:?}", e),
          }
        } else {
          instrument.write_time_diff_to_new_file(dest_file.to_string());
        }
      }

      // Write output
      for v in out.iter_mut() {
        instrument.update();
        *v = instrument.get_sample();
        time += frame_t;
      }

      // Continue as normal
      jack::Control::Continue
      },
	);

  // 4. Activate the client
  let active_client = client.activate_async((), process).unwrap();

	// hook it all up
  // TODO externalize these to config file and have
	// text menu pop up offering options when they fail
  active_client
    .as_client()
    .connect_ports_by_name("vibes:music_out", "system:playback_1")
    .unwrap();
  active_client
    .as_client()
    .connect_ports_by_name("vibes:music_out", "system:playback_2")
    .unwrap();
  active_client
    .as_client()
    .connect_ports_by_name("system:midi_capture_1", "vibes:midi_in")
    .unwrap();

  // wait or do some processing while your handler is running in real time.
  println!("Enter a filename to save your stuff there lol");
  while let Some(s) = read_user_input() {
    tx.send(s).unwrap();
  }

  // Optional deactivate. Not required since active_client will deactivate on
  // drop, though explicit deactivate may help you identify errors in
  // deactivate.
  active_client.deactivate().unwrap();
}

/// Attempt to read a string from standard in. Will block until there is
/// user input. `None` is returned if there was an error reading from standard
/// in, or the retrieved string wasn't a compatible u16 integer.
fn read_user_input() -> Option<String> {
  let mut user_input = String::new();
  match io::stdin().read_line(&mut user_input) {
    Ok(_) => Some(user_input),
      Err(_) => None,
  }
}

