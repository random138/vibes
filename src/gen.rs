
// actual music generation and midi response happens here. All instrument state and syntesis code
// goes in here.

use crate::lib::{World, Time, Kernel, NODES, Bytes64};
use crate::files::{read_file, write_next_number_file, write_file, write_new};


#[derive(Debug, Copy, Clone)]
pub struct Instrument {
  pub time: Time,
  pub base_time: Time,
  pub kern: Kernel,
  pub world: World,
  pub param_arrays: [Time; 9],
  pub params: [f32; 9],
  pub param_scales: [f32; 9],
  pub dest_slider: i32,
}



impl Instrument {
  pub fn update(&mut self) {
    let mut world = self.world;
    let time = self.time;
    let kern = self.kern;
    // take one point-wise time step
    world = world*time;

    // interaction between neighboring cells
    let source_column = 1;
    let dest_column = 0;
    let mut out = world.column(source_column).convolve_same(kern);
    out += world.column(dest_column);
    world.column_mut(dest_column).copy_from(&out);

    // normalize so "energy" is conserved.
    world = world.normalize();
    let old_world = world;
    // and apply a nonlinearity
    world = world.map(|x| { 
      if x > 0.1 {
        return x - 0.2;
      }
      return x;
    });
    // but preserve X's
    let foo = old_world.column(0);
    world.column_mut(0).copy_from(&foo);

    // or apply clip to 1/-1
    //  if x > 1.0 {
    //    return 1.0;
    //  } else if x < -1.0 {
    //    return -1.0;
    //  } else {
    //    return x;
    //  }
    // });
    //let bar = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'.";
    // TODO push a copy of matrix to printing thread instead
    //let bar = ".'`^\",:;Il!i><~+_-?][}{1)(|\\/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao*#MW&8%B@$";
    //for x in world.column(0).iter() {
    //  //let b = ((x + 1.0) * 32.0) as usize;
    //  let b = ((x.abs()) * 64.0) as usize;
    //  print!("{}", &bar[b..b+1]);
    //}
    //println!("");
      
    self.world = world;
  }

  pub fn get_sample(&self) -> f32 {
    let world = self.world;
    return world[(NODES/2,0)];
  }

  pub fn pluck(&mut self, power: f32) {
    self.world[(NODES/2-1, 1)] += power;
  }

  pub fn write_time_diff_to_file(&mut self) {
    let bytes = self.time_diff_as_bytes();
    write_new(&bytes);
  }

  pub fn time_diff_as_bytes(&mut self) -> Bytes64 {
    let mut bytes : [u8; 64] = [0; 64];
    let mut i = 0;
    let time_diff = self.get_time_diff();
    println!("{:?}", time_diff);
    
    for x in time_diff.iter() {
      for b in x.to_be_bytes().iter() {
        bytes[i] = *b;
        i = i + 1;
      }
    }
    return bytes
  }

  pub fn set_dest_slider(&mut self, dest_slider: i32) {
    if (dest_slider < 0) | (dest_slider >= 9) {
      println!("dest slider out of range. Tries {}, should be in [0, 8]. Internally zero based is used, but depending on the connector program you might be using 1 based counting.", dest_slider);
    } else {
      self.dest_slider = dest_slider;
    }
  }

  pub fn read_time_diff_into_slider(&mut self, filename: &str) {
    
    match read_file(filename) {
      Some(bytes) => {
        let mut floats : [f32; 16] = [0.0; 16];
        for i in 0..16 {
          let ls : [u8; 4] = bytes[4*i..4*i+4].try_into().unwrap();
          floats[i] = f32::from_be_bytes(ls);
        }

        self.param_arrays[self.dest_slider as usize] = Time::from_iterator(floats.into_iter());
        println!("{:?}", self.param_arrays[0]);
        self.update_params();
      },
      None => {},
    }
  }

  pub fn write_time_diff_to_new_file(&mut self, filename: String) {
    let bytes = self.time_diff_as_bytes();
    if filename.is_empty() {
      write_next_number_file(&bytes);
    } else {
      write_file(&filename.to_string(), &bytes);
    }
  }


  pub fn get_time_diff(&self) -> Time {
    let mut out = Time::zeros();
    for i in 0..9 {
      out += self.param_scales[i] * self.params[i] * self.param_arrays[i];
    }
    return out;
  }
  

  fn update_params(&mut self) {
    self.time = self.base_time;
    for i in 0..9 {
      self.time += self.param_scales[i] * self.params[i] * self.param_arrays[i];
    }
  }

  pub fn set_param(&mut self, pos: usize, x: f32) {
    self.params[pos] = x;
    self.update_params();
  }
  pub fn set_param_scale(&mut self, pos: usize, x: f32) {
    self.param_scales[pos] = x;
    self.update_params();
  }

  pub fn scramble_param(&mut self, pos: usize) {
    self.param_arrays[pos] = self.param_arrays[pos].map(|_| {
            return 0.1 * (rand::random::<f32>() - 0.5);
            });
    self.update_params();
  }

  pub fn init() -> Instrument {
    let mut world = World::zeros();
    world[(NODES/2-1, 1)] = 1.0;
    let k = 0.02; // spring constant
    let v = 0.00008; // disperse!
    let d = 0.00001; // decay!
    let d2 = 0.00001; // decay 2!
    let kern = Kernel::new(v/2.0, 0.0, v/2.0);
    let time = Time::new(
        1.0-k*k - d, -k,    d,       0.0,
        k,           1.0,   0.0,     0.0,
        0.0,         0.0,   1.0-k*k, -k,  
        0.0,         d2,   k,       1.0-d2);
    let param_arrays = [Time::zeros(); 9];
    let params = [0.0; 9];
    let param_scales = [1.0; 9];

    let mut out = Instrument{ params: params, param_scales: param_scales, param_arrays: param_arrays, base_time: time, time: time, kern: kern, world: world, dest_slider: 0 };
    for pos in 0..9 {
      out.scramble_param(pos as usize);
    }
    return out
  }
}
